<?php

/**
 * @file
 * 
 */

/**
 * Settings form.
 */
function commerce_freeagent_admin_settings() {
  if(TRUE){
    $id = md5(variable_get('freeagent_token_endpoint', 'https://api.sandbox.freeagent.com/v2/token_endpoint') . variable_get('freeagent_api_client_id', '') . 'server-side');
    $tokens = oauth2_client_get_token($id);
    foreach($tokens as $key => $value){
      if(empty($value)) continue;
      variable_set('freeagent_api_' . $key, $value);
    }
  }
  // check if the token need to be refreshed
  if(isset($_GET['code'])){
    try {
      $oauth2_config = array(
        'token_endpoint' => variable_get('freeagent_token_endpoint', 'https://api.freeagent.com/v2/token_endpoint'),
        'auth_flow' => 'server-side',
        'client_id' => variable_get('freeagent_api_client_id', ''),
        'client_secret' => variable_get('freeagent_api_secret', ''),
        'authorization_endpoint' => variable_get('freeagent_authorization_endpoint', 'https://api.freeagent.com/v2/approve_app'),
        'redirect_uri' => url(NULL, array('absolute' => TRUE)) . 'admin/config/services/freeagent-api',
      );
      $oauth2_client = new OAuth2\Client($oauth2_config);
      $oauth2_client->getAccessToken(); // redirect to the success=1
    }
    catch (Exception $e) {
      form_set_error('freeagent_api_client_id', t('We cannot get the access token please contact the administrator.'));
      watchdog('FreeAgent API', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
  }
  // start the form
  $form = array();
  $form['freeagent_api_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('App OAuth Identifier'),
    '#required' => TRUE,
    '#default_value' => variable_get('freeagent_api_client_id', ''),
  );
  
  $form['freeagent_api_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('App OAuth secret'),
    '#required' => TRUE,
    '#default_value' => variable_get('freeagent_api_secret', ''),
  );
  
  $form['freeagent_authorization_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Freeagent authorization endpoint'),
    '#required' => TRUE,
    '#default_value' => variable_get('freeagent_authorization_endpoint', 'https://api.freeagent.com/v2/approve_app'),
  );
  
  $form['freeagent_token_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Freeagent token endpoint'),
    '#required' => TRUE,
    '#default_value' => variable_get('freeagent_token_endpoint', 'https://api.freeagent.com/v2/token_endpoint'),
  );
  
  $form['freeagent_api_redirect_url'] = array(
    '#type' => 'item',
    '#title' => t('App OAuth redirect URI'),
    '#markup' => url('admin/config/services/freeagent-api', array('absolute' => TRUE))
  );
  
  $form['authorization'] = array(
    '#type' => 'fieldset',
    '#title' => t('oAuth2 info'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE
  );
  
  $form['authorization']['freeagent_api_access_token'] = array(
    '#type' => 'item',
    '#title' => t('App OAuth Access Token'),
    '#markup' => variable_get('freeagent_api_access_token', 'No Access Token'),
  );
  
  $form['authorization']['freeagent_api_expires_in'] = array(
    '#type' => 'item',
    '#title' => t('Access Token expired in'),
    '#markup' => format_interval(variable_get('freeagent_api_expires_in', 0)),
  );
  
  $form['authorization']['freeagent_api_refresh_token'] = array(
    '#type' => 'item',
    '#title' => t('Refresh Token'),
    '#markup' => variable_get('freeagent_api_refresh_token', 'No Refresh Token'),
  );
  
  $form['authorization']['freeagent_api_expiration_time'] = array(
    '#type' => 'item',
    '#title' => t('Refresh Token expired in'),
    '#markup' => format_interval(variable_get('freeagent_api_expiration_time', 0)),
  );
  
  $form['authorization']['freeagent_api_token_type'] = array(
    '#type' => 'item',
    '#title' => t('Token type'),
    '#markup' => variable_get('freeagent_api_token_type', 'bearer'),
  );
  
  $form['actions']['authorize'] = array(
    '#type' => 'submit',
    '#value' => t('Authorize'),
    '#access' => !empty(variable_get('freeagent_api_client_id', NULL)) ? TRUE : FALSE
  );
  
  $form['actions']['authorize']['#submit'] = array('commerce_freeagent_admin_settings_authorize_submit');
  
  return system_settings_form($form);
}

/**
 * Settings form validate
 */
function commerce_freeagent_admin_settings_validate($form, &$form_state) {}

/**
 * Settings submit for the authorize
 */
function commerce_freeagent_admin_settings_authorize_submit($form, &$form_state){
  // check if the token need to be refreshed
  if(isset($form_state['values']['freeagent_api_client_id']) && isset($form_state['values']['freeagent_api_secret'])){
    try {
      $oauth2_config = array(
        'token_endpoint' => $form_state['values']['freeagent_token_endpoint'],
        'auth_flow' => 'server-side',
        'client_id' => $form_state['values']['freeagent_api_client_id'],
        'client_secret' => $form_state['values']['freeagent_api_secret'],
        'authorization_endpoint' => $form_state['values']['freeagent_authorization_endpoint'],
        'redirect_uri' => url(NULL, array('absolute' => TRUE)) . 'admin/config/services/freeagent-api',
      );
      $oauth2_client = new OAuth2\Client($oauth2_config);
      $access_token = $oauth2_client->getAccessToken();
      drupal_set_message(t('Authentication was successful.'));
    }
    catch (Exception $e) {
      form_set_error('freeagent_api_client_id', t('We cannot get the access token please contact the administrator.'));
      watchdog('FreeAgent API', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
  }
}