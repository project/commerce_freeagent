FreeAgent Integration
=============

This module integrates Drupal Commerce with the online accounting software, FreeAgent.


Installation
------------

1. Install the module on your site as normal.
2. Create a developer account at https://dev.freeagent.com/apps
3. Create a new app and set the 'Url' to be your website's url. You can leave the 'Oauth redirect url' blank.
4. Go to admin/config/services/freeagent-api and enter in the client id and secret that were generated when you registered your app and click the 'Save Configurations' button. Then 'Authorize' button to authorize the application and get the access tokens.
