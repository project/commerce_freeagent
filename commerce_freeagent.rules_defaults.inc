<?php

/**
 * @file
 * Default rule configurations for Commerce Stock.
 */
function commerce_freeagent_default_rules_configuration() {
  
  $rules_create_a_contact_when_billing_profile_created = '{ "rules_create_a_contact_when_billing_profile_created" : {
    "LABEL" : "Create a Contact when a billing profile created",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "FreeAgent Integration" ],
    "REQUIRES" : [ "rules", "wsclient", "entity" ],
    "ON" : { "commerce_customer_profile_insert" : [] },
    "IF" : [
      { "entity_is_of_bundle" : {
          "entity" : [ "commerce-customer-profile" ],
          "type" : "commerce_customer_profile",
          "bundle" : { "value" : { "billing" : "billing" } }
        }
      },
      { "entity_has_field" : {
          "entity" : [ "commerce-customer-profile" ],
          "field" : "field_freeagent_id"
        }
      }
    ],
    "DO" : [
      { "wsclient_freeagent_api_create_contact" : {
          "USING" : {
            "param_first_name" : "[commerce-customer-profile:commerce-customer-address:first_name]",
            "param_last_name" : "[commerce-customer-profile:commerce-customer-address:last_name]",
            "param_email" : [ "commerce-customer-profile:user:mail" ],
            "param_address1" : [ "commerce-customer-profile:commerce-customer-address:thoroughfare" ],
            "param_region" : [
              "commerce-customer-profile:commerce-customer-address:administrative-area"
            ],
            "param_town" : [ "commerce-customer-profile:commerce-customer-address:locality" ],
            "param_postcode" : [ "commerce-customer-profile:commerce-customer-address:postal-code" ],
            "param_country" : [ "commerce-customer-profile:commerce-customer-address:country" ]
          },
          "PROVIDE" : { "result" : { "result" : "Created Contact Response" } }
        }
      },
      { "data_set" : {
          "data" : [ "commerce-customer-profile:field-freeagent-id" ],
          "value" : [ "result:contact:id" ]
        }
      },
      { "entity_save" : { "data" : [ "commerce-customer-profile" ], "immediate" : "1" } }
    ]
  }
}';
  $configs['rules_create_a_contact_when_billing_profile_created'] = rules_import($rules_create_a_contact_when_billing_profile_created);

  $rules_update_the_contact_when_a_billing_profile_is_updated = '{ "rules_update_the_contact_when_a_billing_profile_is_updated" : {
    "LABEL" : "Update the Contact when a billing profile is updated",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "FreeAgent Integration" ],
    "REQUIRES" : [ "rules", "wsclient", "entity" ],
    "ON" : { "commerce_customer_profile_update" : [] },
    "IF" : [
      { "entity_is_of_bundle" : {
          "entity" : [ "commerce-customer-profile" ],
          "type" : "commerce_customer_profile",
          "bundle" : { "value" : { "billing" : "billing" } }
        }
      },
      { "entity_has_field" : {
          "entity" : [ "commerce-customer-profile" ],
          "field" : "field_freeagent_id"
        }
      }
    ],
    "DO" : [
      { "wsclient_freeagent_api_update_contact" : {
          "USING" : {
            "param_id" : [ "commerce-customer-profile:field-freeagent-id" ],
            "param_first_name" : [ "commerce-customer-profile:commerce-customer-address:first-name" ],
            "param_last_name" : [ "commerce-customer-profile:commerce-customer-address:last-name" ],
            "param_email" : [ "commerce-customer-profile:user:mail" ],
            "param_address1" : [ "commerce-customer-profile:commerce-customer-address:thoroughfare" ],
            "param_region" : [
              "commerce-customer-profile:commerce-customer-address:administrative-area"
            ],
            "param_town" : [ "commerce-customer-profile:commerce-customer-address:locality" ],
            "param_postcode" : [ "commerce-customer-profile:commerce-customer-address:postal-code" ],
            "param_country" : [ "commerce-customer-profile:commerce-customer-address:country" ]
          },
          "PROVIDE" : { "result" : { "result" : "Updated Contact Response" } }
        }
      }
    ]
  }
}';
  $configs['rules_update_the_contact_when_a_billing_profile_is_updated'] = rules_import($rules_update_the_contact_when_a_billing_profile_is_updated);

  $rules_delete_the_contact_when_a_billing_profile_is_deleted = '{ "rules_delete_the_contact_when_a_billing_profile_is_deleted" : {
    "LABEL" : "Delete the Contact when a billing profile is deleted",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "FreeAgent Integration" ],
    "REQUIRES" : [ "rules", "wsclient", "entity" ],
    "ON" : { "commerce_customer_profile_delete" : [] },
    "IF" : [
      { "entity_is_of_bundle" : {
          "entity" : [ "commerce-customer-profile" ],
          "type" : "commerce_customer_profile",
          "bundle" : { "value" : { "billing" : "billing" } }
        }
      }
    ],
    "DO" : [
      { "wsclient_freeagent_api_delete_contact" : {
          "USING" : { "param_id" : [ "commerce-customer-profile:field-freeagent-id" ] },
          "PROVIDE" : { "result" : { "result" : "Deleted Contact Response" } }
        }
      }
    ]
  }
}';
  $configs['rules_delete_the_contact_when_a_billing_profile_is_deleted'] = rules_import($rules_delete_the_contact_when_a_billing_profile_is_deleted);

  $rules_create_an_invoice_when_order_created = '{ "rules_create_an_invoice_when_order_created" : {
    "LABEL" : "Create an Invoice when an Order checkout completed",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "FreeAgent Integration" ],
    "REQUIRES" : [ "rules", "wsclient", "commerce_checkout" ],
    "ON" : { "commerce_checkout_complete" : [] },
    "IF" : [
      { "entity_has_field" : { "entity" : [ "commerce-order" ], "field" : "commerce_customer_billing" } },
      { "entity_has_field" : {
          "entity" : [ "commerce-order:commerce-customer-billing" ],
          "field" : "field_freeagent_id"
        }
      },
      { "entity_has_field" : { "entity" : [ "commerce-order" ], "field" : "field_freeagent_id" } }
    ],
    "DO" : [
      { "variable_add" : {
          "USING" : {
            "type" : "list\u003Cwsclient_freeagent_api_invoice_items\u003E",
            "value" : [ "" ]
          },
          "PROVIDE" : { "variable_added" : { "invoice_items_list" : "Invoice Items List" } }
        }
      },
      { "LOOP" : {
          "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
          "ITEM" : { "line_item" : "Line item" },
          "DO" : [
            { "data_create" : {
                "USING" : {
                  "type" : "wsclient_freeagent_api_invoice_items",
                  "param_description" : [ "line-item:line-item-label" ],
                  "param_item_type" : "Products",
                  "param_price" : [ "line-item:commerce-unit-price:amount-decimal" ],
                  "param_quantity" : [ "line-item:quantity" ]
                },
                "PROVIDE" : { "data_created" : { "invoice_item" : "Invoice Item" } }
              }
            },
            { "list_add" : {
                "list" : [ "invoice-items-list" ],
                "item" : [ "invoice-item" ],
                "unique" : "1"
              }
            }
          ]
        }
      },
      { "wsclient_freeagent_api_create_invoice" : {
          "USING" : {
            "param_contact" : [ "commerce-order:commerce-customer-billing:field-freeagent-id" ],
            "param_dated_on" : [ "commerce-order:created" ],
            "param_payment_terms_in_days" : "5",
            "param_invoice_items" : [ "invoice-items-list" ]
          },
          "PROVIDE" : { "result" : { "result" : "Created Invoice Response" } }
        }
      },
      { "data_set" : {
          "data" : [ "commerce-order:field-freeagent-id" ],
          "value" : [ "result:invoice:id" ]
        }
      },
      { "entity_save" : { "data" : [ "commerce-order" ], "immediate" : "1" } }
    ]
  }
}';
  $configs['rules_create_an_invoice_when_order_created'] = rules_import($rules_create_an_invoice_when_order_created);

  $rules_update_the_invoice_when_an_order_is_updated = '{ "rules_update_the_invoice_when_an_order_is_updated" : {
    "LABEL" : "Update the Invoice when an order is updated",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "FreeAgent Integration" ],
    "REQUIRES" : [ "rules", "wsclient", "entity" ],
    "ON" : { "commerce_order_update" : [] },
    "IF" : [
      { "entity_has_field" : { "entity" : [ "commerce-order" ], "field" : "field_freeagent_id" } },
      { "data_is" : {
          "data" : [ "commerce-order:status" ],
          "op" : "IN",
          "value" : { "value" : {
              "pending" : "pending",
              "processing" : "processing",
              "completed" : "completed"
            }
          }
        }
      }
    ],
    "DO" : [
      { "wsclient_freeagent_api_update_invoice" : {
          "USING" : {
            "param_id" : [ "commerce-order:field-freeagent-id" ],
            "param_payment_terms_in_days" : "5"
          },
          "PROVIDE" : { "result" : { "result" : "Updated Invoice Response" } }
        }
      }
    ]
  }
}';
  $configs['rules_update_the_invoice_when_an_order_is_updated'] = rules_import($rules_update_the_invoice_when_an_order_is_updated);

  $rules_delete_the_invoice_when_an_order_is_deleted = '{ "rules_delete_the_invoice_when_an_order_is_deleted" : {
    "LABEL" : "Delete the Invoice when an Order is deleted",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "FreeAgent Integration" ],
    "REQUIRES" : [ "rules", "wsclient", "entity" ],
    "ON" : { "commerce_order_delete" : [] },
    "IF" : [
      { "entity_has_field" : { "entity" : [ "commerce-order" ], "field" : "field_freeagent_id" } }
    ],
    "DO" : [
      { "wsclient_freeagent_api_delete_invoice" : {
          "USING" : { "param_id" : [ "commerce-order:field-freeagent-id" ] },
          "PROVIDE" : { "result" : { "result" : "Deleted Invoice Response" } }
        }
      }
    ]
  }
}';
  $configs['rules_delete_the_invoice_when_an_order_is_deleted'] = rules_import($rules_delete_the_invoice_when_an_order_is_deleted);

  return $configs;
}